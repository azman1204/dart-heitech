/**
 * function vs method
 * method is function but reside in a class
 */

void main() {
  String greeting = sayHello();
  print("$greeting");
  // toUppercase() - built-in function
  String greeting2 = greeting.toUpperCase();
  print("$greeting2");

  String say = sayHappyBirthday('John Doe', 45);
  print(say);

  print(sayHappyBirthday2('Abu Bakar'));
  print(sayHappyBirthday2('Ali', 60));
  print(sayHappyBirthday3('Ali', age: 50));
  nestedFunction();
}

// custom function / user-defined-function
String sayHello() {
  return 'Hello World!';
}

// positional parameter (compulsory)
String sayHappyBirthday(String name, int age) {
  return "$name is ${age.toString()} years old";
}

// optional parmeter. optional must come after compulsory parameter
String sayHappyBirthday2(String name, [int age = 40]) {
  return "$name is ${age.toString()} years old";
}

// named parameter
String sayHappyBirthday3(String name, {required int age}) {
  return "$name is ${age.toString()} years old";
}

// demo dlm function boleh ada function lain
void nestedFunction() {
  void localFunction() {
    print('this is local function');
  }

  localFunction();
}
