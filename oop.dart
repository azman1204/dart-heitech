// apa beza class (blueprint) dan object
void main() {
  Person p1 = Person('John Doe', 45);
  //p1.age = 45;
  //p1.name = 'John Doe';
  p1.printMe();

  Student stu = Student('Ali', 20);
  //stu.name = 'Ali';
  stu.matric_no = '1234567890';
  stu.printMe();
  stu.printCGPA();
}

class Person {
  // properties
  String? name; // ? = nullable.. boleh ada null value
  int? age;

  // constructor is a method.. ikut nama class
  Person(String name, int age) {
    this.name = name;
    this.age = age;
  }

  // methods
  void printMe() {
    print("Name = ${this.name} Age = ${this.age}");
  }
}

class Student extends Person {
  String? matric_no;
  double cgpa = 3.6;

  // constructor
  Student(String name, int age) : super(name, age);

  void printCGPA() {
    print('${this.matric_no} with CGPA ${this.cgpa}');
  }
}
