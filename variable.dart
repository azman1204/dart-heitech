// dart is OOP lang, semua perkara adalah object
void main() {
  // variable type in dart (static vs dynamic)
  // cth static
  int age = 30;

  // dynamic
  var name = 'Azman'; // infer the type

  double salary = 7500.50;

  String addr = 'Bangi Lama';

  bool is_working = true;

  dynamic elaun = 100;
  elaun = 200.50;

  print('name = $name');
  print('age = ' + age.toString());
}
