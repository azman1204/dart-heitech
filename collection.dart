/*
List, Map, Set
*/

void main() {
  // List
  List<String> fruits = ['Jambu', 'Epal', 'Nangka'];
  fruits.add('Rambutan');
  fruits.remove('Epal');
  for (int i = 0; i < fruits.length; i++) {
    print('Buah ${fruits[i]}');
  }

  // set - element x boleh sama
  Set<int> numbers = {1, 10, 15, 20, 10};
  numbers.forEach((element) {
    print(element);
  });

  // Map - key : value
  Map<String, String> stud = {'name': 'Azman', 'addr': 'Bangi'};
  stud['age'] = '35';

  stud.forEach((key, value) {
    print('key=$key value=$value');
  });
}
