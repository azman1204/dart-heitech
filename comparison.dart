void main() {
  int no1 = 5, no2 = 10;

  if (no1 == no2) {
    print('no1 and no2 equal');
  } else {
    print('no1 and no2 not equal');
  }

  assert(no1 == no2,
      'No1 not equal to no2'); // checking, if not true, print message
  var name = 'azman';

  if (name is String) {
    print('name is String');
  } else {
    print('name is unknown type');
  }

  // >, <, !=, >=, <=
}
