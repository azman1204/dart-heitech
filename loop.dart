void main() {
  // loop 1
  for (int i = 0; i < 10; i++) {
    print("i = ${i + 1}");
  }

  // Loop 2 - khas utk List / Array
  List names = ['Abu', 'Ali', 'Ahmad'];
  for (String name in names) {
    print(name);
  }

  // Loop 3 - khas utk List
  names.forEach((staf) {
    print(staf);
  });

  // Loop 4 - vriant loop no 3
  names.forEach(cetak);

  // Loop 5 - while
  int bil = 0;
  while (true) {
    if (bil++ > 10) break; // break = exit loop. lawan break ialah continue
    print(bil);
  }
}

void cetak(nama) {
  print(nama);
}
